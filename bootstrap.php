<?php

//Load system settings
$system_settings = parse_ini_file(__DIR__ . DIRECTORY_SEPARATOR . sprintf("settings/%s/system.ini", $_SERVER['EASE_ENVIRONMENT']), true);

//Set system time zone
date_default_timezone_set($system_settings['config']['default_timezone']);

//Update system include path
$include_directories = implode(":", $system_settings['include_path']);
$include_paths = array(
    get_include_path(),
    $system_settings['paths']['root'] . DIRECTORY_SEPARATOR . "src",
    $include_directories
);
set_include_path(implode(":", $include_paths));

//Register autoload
spl_autoload_register(function($class) {
    $segments = explode("\\", $class);
    if (is_null($segments[0])) {
        unset($segments[0]);
    }
    $className = array_pop($segments) . ".php";
    $fqcn = strtolower(implode(DIRECTORY_SEPARATOR, $segments)) . DIRECTORY_SEPARATOR . $className;
    include_once $fqcn;
});

//load routing settings
$routing_settings = parse_ini_file(__DIR__ . DIRECTORY_SEPARATOR . sprintf("settings/%s/routes.ini", $_SERVER['EASE_ENVIRONMENT']), true);
$routingManagerClass = $system_settings['config']['routing_manager'];
try {
    $routingManager = new $routingManagerClass();
    $routingManager->setRoutes($routing_settings);
} catch (\Exception $e) {
    throw new \Exception("Could not create routing manager");
}

//Load database settings
if (file_exists(__DIR__ . DIRECTORY_SEPARATOR . sprintf("settings/%s/database.ini", $_SERVER['EASE_ENVIRONMENT']))) {
    $database_settings = parse_ini_file(__DIR__ . DIRECTORY_SEPARATOR . sprintf("settings/%s/database.ini", $_SERVER['EASE_ENVIRONMENT']), true);
    $dbm = \Ease\Utils\DatabaseSettingsManager::getInstance();
    $dbm->loadSettings($database_settings);
}
