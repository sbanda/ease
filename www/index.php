<?php

require_once '../bootstrap.php';


//Create an environment for the request to be handled in
$environment = new Ease\Utils\Environment();

//inject system settings
$environment->setSettings($system_settings);

//inject environment headers
$environment->setHeaders(getallheaders());

//inject server information
$environment->setServer($_SERVER);

//inject GET data
$environment->setGet($_GET);

//inject POST data
$environment->setPost($_POST);

//inject raw POST data
$environment->setRawPostData(file_get_contents("php://input"));

//inject FILES data
$environment->setFiles($_FILES);

//injecting routing manager
$routingManager->setEnvironment($environment);
$environment->setRoutingManager($routingManager);

//Create a response object for the request
$response = new Ease\Requests\Response();

//Create a request handler and inject the environment and response object
$requestHandler = new Ease\Requests\Handlers\EasyHandler($environment, $response);

try {
    echo $requestHandler->getResponse()->setResponseHeaders()->getOutput();
} catch (\Exception $ex) {
    header("HTTP/1.0 500 ".$ex->getMessage());
    echo $ex->getMessage();
}
