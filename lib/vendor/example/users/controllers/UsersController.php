<?php

namespace Example\Users\Controllers;

/**
 * Description of UsersController
 *
 * @author sbanda
 */
class UsersController extends \Ease\Requests\Controllers\EasyController {
    
    static $USERS = [
        [
            'id' => 1,
            'username' => "aBoyNamedSu"
        ],
    ];
    
    public function handleDelete() {
        
    }

    public function handleGet() {
        $this->response->setHeader('Content-Type', "application/json");
        $this->response->setOutput(json_encode(self::$USERS));
        return $this->response;
    }

    public function handlePost() {
        
    }

    public function handlePut() {
        
    }

}
