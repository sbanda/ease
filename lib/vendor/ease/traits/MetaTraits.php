<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Ease\Traits;

/**
 * Description of MetaTraits
 *
 * @author sbanda
 */
trait MetaTraits {
    
    protected $meta;

    public function getMeta($key = null) {
        if (is_null($key)) {
            return $this->meta;
        } else {
            if (array_key_exists($key, $this->meta)) {
                return $this->meta[$key];
            }
        }
    }
    
    public function setMeta(array $meta = null) {
        $this->meta = $meta;
    }

    public function setMetaData($key, $value) {
        $this->meta[$key] = $value;
    }
    
}
