<?php

namespace Ease\Users\Managers\Local;

class UsersManager extends \Ease\Orm\ResourceManager {

    protected function getResourceClassName() {
        return "\Ease\Users\User";
    }

    protected function getTableName() {
        return "Ease__Users";
    }

    protected function getClassIdentifier() {
        return "id";
    }

    protected function getClassProperties() {
        return array(
            'id' => array(
                'name' => 'id',
                'rules' => array(
                    \Ease\Orm\Components\ClassComponent::RULE_READ_ONLY,
                )
            ),
            'username' => array(
                'name' => 'username',
            ),
            'password' => array(
                'name' => 'password',
            ),
            'dateCreated' => array(
                'name' => 'dateCreated',
                'rules' => array(
                    \Ease\Orm\Components\ClassComponent::RULE_CREATE_ONLY,
                ),
                'callbacks' => array(
                    \Ease\Orm\Components\ClassComponent::EVENT_CREATE => 'isCurrentTimeStamp',
                )
            ),
            'lastUpdated' => array(
                'name' => 'lastUpdated',
                'rules' => array(
                    \Ease\Orm\Components\ClassComponent::RULE_UPDATE_ONLY,
                ),
                'callbacks' => array(
                    \Ease\Orm\Components\ClassComponent::EVENT_UPDATE => 'isCurrentTimeStamp'
                )
            ),
            'status' => array(
                'name' => 'status',
            )
        );
    }

    protected function getClassAssociations() {
        return array(
            'meta' => array(
                'name' => 'meta',
                'table' => 'Ease__Users_MetaData',
                'fields' => array(
                    'dataKey',
                    'dataValue',
                ),
                'criteria' => array(
                    'userId' => 'id'
                ),
                'callbacks' => array(
                    \Ease\Orm\Components\ClassComponent::EVENT_CREATE => 'writeFormatMetaData',
                    \Ease\Orm\Components\ClassComponent::EVENT_RETRIEVE => 'readFormatMetaData',
                ),
                'searchRules' => array(
                    'keyField' => 'dataKey',
                    'valueField' => 'dataValue'
                ),
            )
        );
    }

    protected function getClassCollections() {
        return array();
    }

}
