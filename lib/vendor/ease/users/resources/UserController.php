<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Ease\Users\Resources;

/**
 * Description of UserController
 *
 * @author sbanda
 */
class UserController extends \Ease\Requests\Controllers\ResourceController {

    protected static $requiredFields = array(
        'username',
        'password',
        'status',
    );

    public function handleDelete() {
        
    }

    public function handleGet() {
        if (!$this->environment->getParam('id')) {
            throw new \Exception("No ID specified");
        }
        $user = $this->getResourceManager()->retrieve(array('id' => $this->environment->getParam('id')));
        $this->response->setHeader('Content-Type', "application/json");
        $this->response->setOutput($user->render());
        return $this->response;
    }

    public function handlePost() {
        $payload = json_decode($this->environment->getRawPostdata(), true);
        $this->validateInput($payload);

        $usersManager = $this->getResourceManager();
        $user = new \Ease\Users\User();
        $user->setUsername($payload['username']);
        $user->setPassword(sha1($payload['password']));
        $user->setStatus($payload['status']);
        
        if (array_key_exists('meta', $payload)) {
            $user->setMeta($payload['meta']);
        }
        
        try {
            $usersManager->create($user);
        } catch (\Exception $e) {
            error_log("Failed to create user: ".$e->getMessage());
            throw new \Exception("Could not create user");
        }
        
        $this->response->setHeader('Content-Type', "application/json");
        $this->response->setOutput($user->render());
        return $this->response;
    }

    public function handlePut() {
        if (!$this->environment->getParam('id')) {
            throw new \Exception("No ID specified");
        }

        $usersManager = $this->getResourceManager();
        $user = $usersManager->retrieve(array('id' => $this->environment->getParam('id')));
        
        $payload = json_decode($this->environment->getRawPostdata(), true);
        $this->validateInput($payload);
        
        //update properties if set
        
        $this->response->setHeader('Content-Type', "application/json");
        $this->response->setOutput($user->render());
        return $this->response;
    }

    protected function getResourceManager() {
        $dbm = \Ease\Utils\DatabaseSettingsManager::getInstance();
        $db = $dbm->getConnection('default');
        return new \Ease\Users\Managers\Local\UsersManager($db);
    }

    protected function validateInput(array $input) {
        foreach (self::$requiredFields as $property) {
            $missingFields = array();
            if (!array_key_exists($property, $input)) {
                $missingFields[] = $property;
            }
        }

        if (count($missingFields)) {
            $fields = \implode(", ", $missingFields);
            throw new \Exception("Missing required field: $fields");
        }
        
        return true;
    }

}
