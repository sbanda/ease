<?php

namespace Ease\Users;

class User extends \Ease\Resources\Resource {
    
    use \Ease\Traits\MetaTraits;

    const STATUS_ACTIVE = "ACTIVE";
    const STATUS_INACTIVE = "INACTIVE";
    const STATUS_BLOCKED = "BLOCKED";

    protected static $USER_STATUSES = array(
        self::STATUS_ACTIVE,
        self::STATUS_INACTIVE,
        self::STATUS_BLOCKED
    );
    
    protected $id;
    protected $username;
    protected $password;
    protected $dateCreated;
    protected $lastUpdated;
    protected $status;

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getUsername() {
        return $this->username;
    }

    public function setUsername($username) {
        $this->username = $username;
    }

    public function getPassword() {
        return $this->password;
    }

    public function setPassword($password) {
        $this->password = $password;
    }

    public function getDateCreated() {
        return $this->dateCreated;
    }

    public function setDateCreated($date) {
        $this->dateCreated = $date;
    }

    public function getLastUpdated() {
        return $this->lastUpdated;
    }

    public function setLastUpdated($date) {
        $this->lastUpdated = $date;
    }

    public function getStatus() {
        return $this->status;
    }

    public function setStatus($status) {
        if (in_array($status, self::$USER_STATUSES)) {
            $this->status = $status;
        } else {
            throw new \Exception("Unkown Status");
        }
    }

}
