<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Ease\Auth\Managers\Local;

/**
 * Description of ServiceProvidersManager
 *
 * @author sbanda
 */
class ServiceProvidersManager extends \Ease\Orm\ResourceManager {

    protected function getResourceClassName() {
        return "\Ease\Auth\ServiceProvider";
    }

    protected function getTableName() {
        return "Ease__Auth_ServiceProviders";
    }

    protected function getClassIdentifier() {
        return "id";
    }

    protected function getClassProperties() {
        return array(
            'id' => array(
                'name' => 'id',
                'rules' => array(
                    \Ease\Orm\Components\ClassComponent::RULE_READ_ONLY,
                ),
            ),
            'name' => array(
                'name' => 'name',
            ),
            'status' => array(
                'name' => 'status',
            ),
        );
    }

    protected function getClassAssociations() {
        return array(
            'meta' => array(
                'name' => "meta",
                'table' => "Ease__Auth_ServiceProviders_MetaData",
                'fields' => array(
                    'dataKey',
                    'dataValue'
                ),
                'criteria' => array(
                    'providerId' => 'id'
                ),
                'callbacks' => array(
                    \Ease\Orm\Components\ClassComponent::EVENT_CREATE => 'writeFormatMetaData',
                    \Ease\Orm\Components\ClassComponent::EVENT_RETRIEVE => 'readFormatMetaData',
                ),
                'default' => []
            )
        );
    }

    protected function getClassCollections() {
        return array();
    }

}
