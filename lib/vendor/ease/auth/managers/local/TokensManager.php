<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Ease\Auth\Managers\Local;

/**
 * Description of TokensManager
 *
 * @author sbanda
 */
class TokensManager extends \Ease\Orm\ResourceManager {

    protected function getResourceClassName() {
        return "\Ease\Auth\Token";
    }

    protected function getTableName() {
        return "Ease__Auth_Tokens";
    }

    protected function getClassIdentifier() {
        return "id";
    }

    protected function getClassProperties() {
        return array(
            'id' => array(
                'name' => 'id',
                'rules' => array(
                    \Ease\Orm\Components\ClassComponent::RULE_READ_ONLY,
                ),
            ),
            'token' => array(
                'name' => 'token',
            ),
            'providerId' => array(
                'name' => 'providerId',
            ),
            'userId' => array(
                'name' => 'userId',
            ),
            'type' => array(
                'name' => 'type',
                'field' => 'tokenType',
            ),
            'dateCreated' => array(
                'name' => 'dateCreated',
                'rules' => array(
                    \Ease\Orm\Components\ClassComponent::RULE_CREATE_ONLY,
                ),
                'callbacks' => array(
                    \Ease\Orm\Components\ClassComponent::EVENT_CREATE => 'isCurrentTimeStamp',
                ),
            ),
            'lastAccessed' => array(
                'name' => 'lastAccessed',
                'rules' => array(
                    \Ease\Orm\Components\ClassComponent::RULE_UPDATE_ONLY,
                ),
                'callbacks' => array(
                    \Ease\Orm\Components\ClassComponent::EVENT_UPDATE => 'isCurrentTimeStamp',
                )
            ),
            'expiryDate' => array(
                'name' => 'expiryDate',
            ),
        );
    }

    protected function getClassAssociations() {
        return array(
            'meta' => array(
                'name' => 'meta',
                'table' => 'Ease__Auth_Tokens_MetaData',
                'fields' => array(
                    'dataKey',
                    'dataValue'
                ),
                'criteria' => array(
                    'tokenId' => 'id',
                ),
                'callbacks' => array(
                    \Ease\Orm\Components\ClassComponent::EVENT_CREATE => 'writeFormatMetaData',
                    \Ease\Orm\Components\ClassComponent::EVENT_RETRIEVE => 'readFormatMetaData',
                ),
                'default' => [],
            ),
        );
    }

    protected function getClassCollections() {
        return array();
    }

}
