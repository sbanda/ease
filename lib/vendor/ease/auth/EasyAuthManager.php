<?php

namespace Ease\Auth;

/**
 * Description of EasyAuthManager
 *
 * @author sbanda
 */
class EasyAuthManager {

    /**
     *
     * @var \Ease\Utils\Environment
     */
    static protected $environment;
    
    /**
     *
     * @var \Ease\Database\MySQLWrapper
     */
    protected $db;
    
    /**
     *
     * @var ServiceProvider
     */
    protected $serviceProvider;
    
    /**
     *
     * @var \Ease\Users\User
     */
    protected $user;
    
    public function __construct(\Ease\Utils\Environment $environment) {
        self::$environment = $environment;
        $this->init();
    }
    
    protected function init() {
        $authenticationDatabase = self::$environment->getAuthSetting('authentication_database');
        $this->db = \Ease\Utils\DatabaseSettingsManager::getInstance()->getConnection($authenticationDatabase);
    }
    
    public function getEnvironment() {
        return self::$environment;
    }
    
    public function authenticateRequest() {
        $this->authenticateServiceProvider()
             ->authenticateUser();
    }
    
    public function authenticateServiceProvider() {
        error_log("Verifying service provider");
        $serviceProviderHeader = self::$environment->getSystemHeader('service_provider');
        error_log("Service Provider Header: $serviceProviderHeader");
        $serviceProvider = self::$environment->getHeader($serviceProviderHeader);
        if (!$serviceProvider) {
            throw new \Exception("Cannot proceed with request: no service provider specified");
        }
        
        $serviceProvidersManager = new Managers\Local\ServiceProvidersManager($this->db);
        try {
            $this->serviceProvider = $serviceProvidersManager->retrieve(['name' => $serviceProvider]);
            error_log("Service Provider: " . $this->serviceProvider->getId());
            if ($this->serviceProvider->getStatus() !== ServiceProvider::STATUS_ACTIVE) {
                throw new \Exception("Cannot proceed with request: service provider is inactive");
            }
        } catch (\Exception $e) {
            error_log($e->getMessage());
            throw new \Exception("Unknown service provider: $serviceProvider");
        }
        
        self::$environment->set('serviceProvider', $this->serviceProvider);
        error_log("Service Provider successfully verified");
        return $this;
    }
    
    public function authenticateUser() {
        error_log("Verifying user");
        $userNameHeader = self::$environment->getSystemHeader('user_name');
        error_log("User Name Header: $userNameHeader");
        $apiUsername = self::$environment->getHeader($userNameHeader);
        
        $credentialName = self::$environment->getAuthSetting('authentication_credentials_name');
        error_log("User Name Credential: $credentialName");
        $criteria = [
            'dataKey' => $credentialName,
            'dataValue' => $apiUsername
        ];
        
        $userId = $this->db->fetchField("Ease__Users_Credentials", 'userId', $criteria);
        if (!$userId) {
            throw new \Exception("Could not identifiy user");
        }
        
        $usersManager = new \Ease\Users\Managers\Local\UsersManager($this->db);
        try {
            $this->user = $usersManager->retrieve(['id' => $userId]);
            error_log("User: " . $this->user->getId());
        } catch (\Exception $e) {
            error_log($e->getMessage());
            throw new \Exception("Could not identify user");
        }
        
        self::$environment->set('authenticatedUser', $this->user);
        error_log("User successfully verified");
        return $this;
    }
    
}
