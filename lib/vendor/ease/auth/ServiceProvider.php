<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Ease\Auth;

/**
 * Description of ServiceProvider
 *
 * @author sbanda
 */
class ServiceProvider extends \Ease\Resources\Resource {
    
    use \Ease\Traits\MetaTraits;
    
    const STATUS_ACTIVE = 'ACTIVE';
    const STATUS_INACTIVE = 'INACTIVE';
    
    protected $id;
    protected $name;
    protected $status;
    
    public function getId() {
        return $this->id;
    }

    public function getName() {
        return $this->name;
    }

    public function getStatus() {
        return $this->status;
    }

    public function setId($id) {
        $this->id = (int) $id;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setStatus($status) {
        switch (strtoupper($status)) {
            case self::STATUS_ACTIVE:
            case self::STATUS_INACTIVE:
                $this->status = strtoupper($status);
                break;
            default:
                $this->status = self::STATUS_INACTIVE;
        }
    }

}
