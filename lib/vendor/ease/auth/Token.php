<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Ease\Auth;

/**
 * Description of Token
 *
 * @author sbanda
 */
class Token extends \Ease\Resources\Resource {
    
    const TYPE_SESSION = 'SESSION';
    
    use \Ease\Traits\MetaTraits;
    
    protected $id;
    protected $token;
    protected $providerId;
    protected $userId;
    protected $type;
    protected $dateCreated;
    protected $lastAccessed;
    protected $expiryDate;
    
    public function getId() {
        return $this->id;
    }

    public function getToken() {
        return $this->token;
    }

    public function getProviderId() {
        return $this->providerId;
    }

    public function getUserId() {
        return $this->userId;
    }

    public function getType() {
        return $this->type;
    }

    public function getDateCreated() {
        return $this->dateCreated;
    }

    public function getLastAccessed() {
        return $this->lastAccessed;
    }

    public function getExpiryDate() {
        return $this->expiryDate;
    }

    public function setId($id) {
        $this->id = (int) $id;
    }

    public function setToken($token) {
        $this->token = $token;
    }

    public function setProviderId($providerId) {
        $this->providerId = (int) $providerId;
    }

    public function setUserId($userId) {
        $this->userId = (int) $userId;
    }

    public function setType($type) {
        switch (strtoupper($type)) {
            case self::TYPE_SESSION:
                $this->type = strtoupper($type);
                break;
            default:
                $this->type = self::TYPE_SESSION;
        }
    }

    public function setDateCreated($dateCreated) {
        $this->dateCreated = (int) $dateCreated;
    }

    public function setLastAccessed($lastAccessed) {
        $this->lastAccessed = (int) $lastAccessed;
    }

    public function setExpiryDate($expiryDate) {
        $this->expiryDate = (int) $expiryDate;
    }

}
