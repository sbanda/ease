<?php

/**
 * Description of MySQLWrapper
 *
 * @author Suhmayah Banda
 * @package db
 */

namespace Ease\Database;

class MySQLWrapper {

    protected $server;
    protected $user;
    protected $password;
    protected $db;

    protected $extras;

    /**
     *
     * @var \mysqli
     */
    protected $dbConn;

    /**
     *
     * @var \mysqli_result
     */
    protected $result;

    protected $queryLog;

    const INSERT = 1;
    const INSERT_IGNORE = 2;
    const REPLACE = 3;
    const SELECT = 4;
    const UPDATE = 5;
    const DELETE = 6;

    protected static $OPERATORS = array(
            '_eq' => 'handleEqualToOperator',
            '_ne' => 'handleNotEqualToOperator',
            '_gt' =>  'handleGreaterThanOperator',
            '_gte' => 'handleGreaterThanOrEqualToOperator',
            '_lt' => 'handleLessThanOperator',
            '_lte' => 'handleLessThanOrEqualToOperator',
            '_in' => 'handleInOperator',
            '_nin' => 'handleNotInOperator',
            '_null' => 'handleNullOperator',
            '_nnull' => 'handleNotNullOperator',
            '_like' => 'handleLikeOperator',
            '_nlike' => 'handleNotLikeOperator',
    );

    /**
     * Generic insert query for single and multiple inserts.
     *
     * @var String
     */
    protected static $TPL_INSERT_QUERY = "INSERT INTO __TABLE__ (__FIELDS__) VALUES __VALUES__";

    /**
     * Generic insert ignore query for single and multiple inserts.
     *
     * @var String
     */
    protected static $TPL_INSERT_IGNORE_QUERY = "INSERT IGNORE INTO __TABLE__ (__FIELDS__) VALUES __VALUES__";

    /**
     * Generic replace query for single and multiple inserts.
     *
     * @var String
     */
    protected static $TPL_REPLACE_QUERY = "REPLACE INTO __TABLE__ (__FIELDS__) VALUES __VALUES__";

    /**
     * Generic select query for single and inner joins.
     *
     * @var String
     */
    protected static $TPL_SELECT_QUERY = "SELECT __FIELDS__ FROM __TABLES__ __WHERE__ __ORDER__ __GROUP__ __LIMIT__";

    /**
     * Generic update query
     *
     * @var String
     */
    protected static $TPL_UPDATE_QUERY = "UPDATE __TABLE__ SET __UPDATE_VALUES__ __WHERE__ __LIMIT__";

    /**
     * Generic delete query
     *
     * @var String
     */
    protected static $TPL_DELETE_QUERY = "DELETE FROM __TABLE__ __WHERE__ __LIMIT__";

    public function __construct($credentials) {
        if (!is_array($credentials)) {
            throw new \Exception("No Database Credentials provided.");
        }
        $this->init($credentials);
    }

    protected function init($credentials) {
        foreach (array('server', 'user', 'password', 'db')
                as $required) {
            if (array_key_exists($required, $credentials)) {
                $this->$required = $credentials[$required];
                unset($credentials[$required]);
            } else {
                throw new \Exception("Missing Required Database Credentials:" . $required);
            }
            if (count($credentials)) {
                $this->extras = $credentials;
            }
        }
        $this->connect();
    }

    public function getExtras() {
        if (is_array($this->extras)) {
            return $this->extras;
        }
    }

    /**
     *
     * @throws \Exception
     */
    protected function connect() {
        if (!$this->dbConn = new \mysqli($this->server, $this->user, $this->password, $this->db)) {
            throw new \Exception($this->dbConn->connect_errno.": ".$this->dbConn->connect_error);
        }
        return $this;
    }

    public function execute($sql) {
        $sql = trim($sql);
        $this->queryLog[] = $sql;

        if ($this->result = $this->dbConn->query($sql)) {
            if (strtoupper(substr($sql, 0, 6)) == "INSERT") {
                return $this->dbConn->insert_id;
            }
            return $this->dbConn->affected_rows;
        }
        $this->queryLog[] = $this->getMySQLError();
        throw new \Exception($sql." -- ".$this->getMySQLError());
    }

    /**
     * Execute an insert query.
     *
     * @param string $tableName
     * @param array $values
     * @param int $insertType INSERT, INSERT_IGNORE or REPLACE
     * @return int returns the last insert ID if successfully executed
     */
    public function insert($tableName, array $values, $insertType = MySQLWrapper::INSERT) {
        $queryComponents = array();
        $queryComponents['__TABLE__'] = $tableName;
        $queryComponents['__FIELDS__'] = implode(", ", array_keys($values));
        $escapedValues = array();
        foreach ($values as $value) {
            if (is_null($value)) {
                $escapedValues[] = 'NULL';
            } else {
                $escapedValues[] = '"'.$this->dbConn->real_escape_string($value).'"';
            }
        }
        $queryComponents['__VALUES__'] = "(".implode(", ", $escapedValues).")";
        return $this->execute($this->buildQuery($insertType, $queryComponents));
    }

    /**
     * Execute an insert query.
     *
     * @param string $tableName
     * @param array $values
     * @return int returns the last insert ID if successfully executed
     */
    public function insertMultiple($tableName, $fields, array $values, $insertType = MySQLWrapper::INSERT) {
        $queryComponents = array();
        $queryComponents['__TABLE__'] = $tableName;
        $queryComponents['__FIELDS__'] = implode(", ", $fields);
        $queryComponents['__VALUES__'] = $this->buildValueBlocks($values);
        return $this->execute($this->buildQuery($insertType, $queryComponents));
    }

    /**
     * Performs a select query on a comma separated list of tables.
     *
     * @param string $table A comma separated list of tables on which to perform the query
     * @param string $fields A comma separated list of fields to select
     * @param array $where An array mapping fields to values for the where clause
     * @param string $order The ordering instructions, excluding ORDER BY
     * @param string $group The grouping instructions, excluding GROUP BY
     * @param int $limit
     * @param int $offset
     * @return boolean
     */
    public function select($table, $fields = "*", array $where = null, $order = null, $group = null, $limit = null, $offset = null) {
        $queryComponents = array();
        $queryComponents['__FIELDS__'] = $fields;
        $queryComponents['__TABLES__'] = $table;
        $queryComponents['__WHERE__'] = $this->buildWhereClause($where);
        $queryComponents['__ORDER__'] = $this->getOrderByString($order);
        $queryComponents['__GROUP__'] = $this->getGroupByString($group);
        $queryComponents['__LIMIT__'] = $this->getLimitString($limit, $offset);
        return $this->execute($this->buildQuery(self::SELECT, $queryComponents));
    }

    /**
     * Perform an update query on a table
     *
     * @param string $table The table name on which to perform the query
     * @param array $values An associative array mapping table fields to updated values
     * @param array $where An array mapping fields to values for the where clause
     * @param int $limit
     * @param int $offset
     * @return int The number of rows affected
     */
    public function update($table, $values, $where = null, $limit = null, $offset = null) {
        $queryComponents = array();
        $queryComponents['__TABLE__'] = $table;
        $queryComponents['__UPDATE_VALUES__'] = $this->buildSetClauses($values);
        $queryComponents['__WHERE__'] = $this->buildWhereClause($where);
        $queryComponents['__LIMIT__'] = $this->getLimitString($limit, $offset);
        return $this->execute($this->buildQuery(self::UPDATE, $queryComponents));
    }

    /**
     * Perform a delete query on a table
     *
     * @param string $table
     * @param array $where An array mapping fields to values for the where clause
     * @param int $limit
     * @param int $offset
     */
    public function delete($table, array $where = null, $limit = null, $offset = null) {
        $queryComponents = array();
        $queryComponents['__TABLE__'] = $table;
        $queryComponents['__WHERE__'] = $this->buildWhereClause($where);
        $queryComponents['__LIMIT__'] = $this->getLimitString($limit, $offset);
        return $this->execute($this->buildQuery(self::DELETE, $queryComponents));
    }

    /**
     * Returns the number of rows in the result set for the last db query
     *
     * @return int
     */
    public function getNumRows() {
        if ($this->result) {
            return $this->result->num_rows;
        }
    }

    /**
     * Fetches the first row in the result set of the last mysql query and returns an associative array
     *
     * @return mixed array or boolean
     */
    public function fetchRow() {
        if ($this->getNumRows()) {
            return $this->result->fetch_assoc();
        }
        return false;
    }

    /**
     * Returns the result set of the last query in a multidemnsional associative array
     *
     * @return mixed array or boolean if no row to return
     */
    public function fetchAll($index = null) {
        if ($this->getNumRows()) {
            $rows = array();
            while ($row = $this->result->fetch_assoc()) {
                if (!is_null($index) && array_key_exists($index, $row)) {
                    $rows[$row[$index]] = $row;
                } else {
                    $rows[] = $row;
                }
            }
            $this->result->data_seek(0);
            return $rows;
        }
        return false;
    }

    /**
     * Performs a select query and returns the value of the field specified if found.
     *
     * @see MySQLWrapper::select
     * @param string  $table
     * @param string $field
     * @param array $where
     * @param array $order
     * @param string $group
     * @param int $limit
     * @param int $offset
     * @return mixed or false if no result found
     */
    public function fetchField($table, $field, array $where, $order = null, $group = null, $limit = null, $offset = null) {
        if ($this->select($table, $field, $where, $order, $group, $limit, $offset)) {
            $row = $this->fetchRow();
            return $row[$field];
        }
        return false;
    }
    
    public function fetchCount($table, $field, array $where = null) {
        $this->select($table, "COUNT($field) AS fieldCount", $where);
        return array_shift($this->fetchRow());
    }

    protected function buildWhereClause(array $whereArray = null) {
        if (empty($whereArray)) {
            return "";
        }
        $whereClauses = array();
        foreach ($whereArray as $field => $value) {
            if (!is_array($value)) {
                $whereClauses[] = $field . " = '".$this->dbConn->real_escape_string($value)."'";
            } else if (($clause = $this->getFilter($field, $value))) {
                $whereClauses[] = $clause;
            }
        }
        return (!empty($whereClauses))? " WHERE " . implode(" AND ", $whereClauses) : null;
    }

    protected function getFilter($field, $filter) {
        $filters = array();
        $combinedFilter = null;
        foreach ($filter as $index => $value) {
            if (is_numeric($index)) {
                $partialFilter = $this->getFilter($field, $value);
            } else {
                if (!array_key_exists($index, self::$OPERATORS)) {
                    throw new \Exception('Unknown Operator: '.$index);
                }
                $filterHandler = self::$OPERATORS[$index];
                $partialFilter = $this->$filterHandler($field, $value);
            }
            if (!empty($partialFilter)) {
                $filters[] = $partialFilter;
            }
        }
        if (!empty($filters)) {
            $combinedFilter = implode(" AND ", $filters);
        }
        return $combinedFilter;
    }
    
    protected function handleEqualToOperator($field, $value) {
        $placeHolders = array(
                $field,
                $this->dbConn->real_escape_string($value),
        );
        return vsprintf("%s = '%s'", $placeHolders);
    }

    protected function handleNotEqualToOperator($field, $value) {
        $placeHolders = array(
                $field,
                $this->dbConn->real_escape_string($value),
        );
        return vsprintf("%s != '%s'", $placeHolders);
    }

    protected function handleInOperator($field, array $values) {
        if (empty($values)) {
            return null;
        }
        $escapedValues = array();
        foreach ($values as $value) {
            $escapedValues[] = '"'.$this->dbConn->real_escape_string($value).'"';
        }
        $placeholders = array(
                $field,
                implode(', ', $escapedValues),
        );
        return vsprintf("%s IN (%s)", $placeholders);
    }

    protected function handleNotInOperator($field, array $values) {
        if (empty($values)) {
            return null;
        }
        $escapedValues = array();
        foreach ($values as $value) {
            $escapedValues[] = '"'.$this->dbConn->real_escape_string($value).'"';
        }
        $placeholders = array(
                $field,
                implode(', ', $escapedValues),
        );
        return vsprintf("%s NOT IN (%s)", $placeholders);
    }

    protected function handleGreaterThanOperator($field, $value) {
        $placeHolders = array(
                $field,
                $this->dbConn->real_escape_string($value),
        );
        return vsprintf("%s > '%s'", $placeHolders);
    }

    protected function handleGreaterThanOrEqualToOperator($field, $value) {
        $placeHolders = array(
                $field,
                $this->dbConn->real_escape_string($value),
        );
        return vsprintf("%s >= '%s'", $placeHolders);
    }

    protected function handleLessThanOperator($field, $value) {
        $placeHolders = array(
                $field,
                $this->dbConn->real_escape_string($value),
        );
        return vsprintf("%s < '%s'", $placeHolders);
    }

    protected function handleLessThanOrEqualToOperator($field, $value) {
        $placeHolders = array(
                $field,
                $this->dbConn->real_escape_string($value),
        );
        return vsprintf("%s <= '%s'", $placeHolders);
    }

    protected function handleNullOperator($field, $value) {
        return sprintf("%s IS NULL", $field);
    }

    protected function handleNotNullOperator($field, $value) {
        return sprintf("%s IS NOT NULL", $field);
    }

    protected function handleLikeOperator($field, $value) {
        $placeHolders = array(
                $field,
                $this->dbConn->real_escape_string($value),
        );
        return vsprintf("%s LIKE '%s'", $placeHolders);
    }

    protected function handleNotLikeOperator($field, $value) {
        $placeHolders = array(
                $field,
                $this->dbConn->real_escape_string($value),
        );
        return sprintf("%s NOT LIKE '%s'", $placeHolders);
    }

    protected function buildSetClauses(array $updateValues) {
        $setClauses = array();
        foreach ($updateValues as $field => $value) {
            if (is_null($value)) {
                $setClauses[] = $field . " = NULL";
            } else {
                $setClauses[] = $field . " = '".$this->dbConn->real_escape_string($value)."'";
            }
        }
        return implode(", ", $setClauses);
    }

    protected function getOrderByString($order = null) {
        if (!isset($order)) {
            return "";
        }
        if (is_array($order)) {
            $orders = array();
            foreach ($order as $key => $sorting) {
                if (in_array(strtoupper($sorting), array('ASC', 'DESC'))) {
                    $orders[$key] = sprintf("%s %s", $key, $sorting);
                } else {
                    throw new \Exception("Invalid Sort on $key: $sorting");
                }
            }
            $order = implode(", ", $orders);
        }
        return "ORDER BY ".$order;
    }

    protected function getGroupByString($group = null) {
        if (isset($group)) {
            return "GROUP BY ".$group;
        }
        return "";
    }

    protected function getLimitString($limit = null, $offset = null) {
        if (isset($limit)) {
            $limitString = "LIMIT ";
            if (isset($offset)) {
                $limitString .= $offset.", ";
            }
            $limitString .= $limit;
            return $limitString;
        }
        return "";
    }

    protected function buildQuery($type, array $queryComponents) {
        $queryTpl = $this->getQueryTemplate($type);
        foreach ($queryComponents as $key => $value) {
            $queryTpl = str_replace($key, $value, $queryTpl);
        }
        return $queryTpl;
    }

    protected function getQueryTemplate($type) {
        switch ($type) {
            case self::INSERT:
                return $queryTpl = self::$TPL_INSERT_QUERY;
                break;
            case self::INSERT_IGNORE:
                return $queryTpl = self::$TPL_INSERT_IGNORE_QUERY;
                break;
            case self::REPLACE:
                return $queryTpl = self::$TPL_REPLACE_QUERY;
                break;
            case self::SELECT:
                return self::$TPL_SELECT_QUERY;
                break;
            case self::UPDATE:
                return self::$TPL_UPDATE_QUERY;
                break;
            case self::DELETE:
                return self::$TPL_DELETE_QUERY;
                break;
            default :
                throw new \Exception("Unknown Query Type: ".$type);
                break;
        }
    }

    public function buildValueBlocks(array $values) {
        $valueBlocks = array();
        foreach ($values as $value) {
            $escapedValues = array();
            foreach ($value as $v) {
                if (is_null($v)) {
                    $escapedValues[] = 'NULL';
                } else {
                    $escapedValues[] = '"'.$this->dbConn->real_escape_string($v).'"';
                }
            }
            $valueBlocks[] = "(".implode(", ", $escapedValues).")";
        }
        return implode(", ", $valueBlocks);
    }

    public function getInsertId() {
        return $this->dbConn->insert_id;
    }

    public function getQueryLog() {
        return $this->queryLog;
    }

    private function getMySQLError() {
        return $this->dbConn->errno.": ".$this->dbConn->error;
    }
    
    public function getErrNo() {
    	return $this->dbConn->errno;
    }
    
    public function getError() {
    	return $this->dbConn->error;
    }

    public function reset($offset = 0) {
        $this->result->data_seek($offset);
    }

    protected function close() {
        if ($this->dbConn instanceof \mysqli) {
            $this->dbConn->close();
        }
    }

    public function __destruct() {
        $this->close();
    }

}

