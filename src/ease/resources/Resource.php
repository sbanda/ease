<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Ease\Resources;

/**
 * Description of Resource
 *
 * @author sbanda
 */
abstract class Resource {

    const FORMAT_JSON = 'JSON';

    protected static $INTERNAL = array();
    
    public function __construct() {
        $this->init();
    }
    
    protected function init() {
        self::$INTERNAL[get_called_class()] = array();
    }

    public function getProperties() {
        $properties = get_object_vars($this);
        if (array_key_exists(get_called_class(), self::$INTERNAL)) {
            foreach (self::$INTERNAL[get_called_class()] as $internal) {
                if (array_key_exists($internal, $properties)) {
                    unset($properties[$internal]);
                }
            }
        }        
        return $properties;
    }
    
    public function export(array $mask = null) {
        $properties = $this->getProperties();
        if (is_array($mask)) {
            foreach ($mask as $maskedProperty) {
                if (array_key_exists($maskedProperty, $properties)) {
                    unset($properties[$maskedProperty]);
                }
            }
        }
        return $properties;
    }

    public function render($format = self::FORMAT_JSON, array $mask = null) {
        switch ($format) {
            case self::FORMAT_JSON:
                return $this->renderJSON($mask);
            default:
                throw new \Exception("Unsupported render format: $format");
        }
    }
    
    protected function renderJSON(array $mask = null) {
        $export = $this->export($mask);
        return json_encode($export);
    }
    
    protected function addInternalProperty($name) {
        self::$INTERNAL[get_called_class()][] = $name;
    }
    
    protected function removeInternalProperty($name) {
        if (array_key_exists($name, self::$INTERNAL[get_called_class()])) {
            unset(self::$INTERNAL[get_called_class()][$name]);
        }
    }

}
