<?php

namespace Ease\Orm\Components;

/**
 * ClassComponent
 *
 * @author Bandu
 */
abstract class ClassComponent {
    
    const RULE_READ_ONLY = 1;
    const RULE_CREATE_ONLY = 2;
    const RULE_UPDATE_ONLY = 3;
    
    const EVENT_CREATE = 'create';
    const EVENT_RETRIEVE = 'retrieve';
    const EVENT_UPDATE = 'update';
    const EVENT_DELETE = 'delete';
    
    /**
     * The identifier for this component
     *
     * @var string
     */
    protected $name;

    /**
     * Every element in this array contains a rule the component is subject to
     * 
     * @var array
     */
    protected $rules;
    
    /**
     * An key/value array mapping an event to a function
     * 
     * @var array 
     */
    protected $callbacks;
    
    /*
     * The default value to be used if no value is retrieved from the database
     * 
     * @var mixed
     */
    protected $default;

    public function __construct(array $properties = null) {
        $this->rules = array();
        $this->callbacks = array();
        
        if ($properties) {
            foreach ($properties as $property => $value) {
                $setter = "set".ucfirst($property);
                $this->$setter($value);
            }
        }
    }
    
    /**
     * Determines if the component was created correctly
     * 
     * @return boolean Returns true if all required data is defined, false otherwise
     */
    abstract public function isValid();

    public function setName($name) {
        if (!empty($name)) {
            $this->name = $name;
        }
    }
    
    public function getName() {
        return $this->name;
    }
    
    public function setRules(array $rules) {
        if (!empty($rules)) {
            $this->rules = $rules;
        }
    }
    
    public function setRule($name) {
        $this->rules[] = $name;
    }
    
    public function getRules() {
        return $this->rules;
    }
    
    public function setDefault($default) {
        $this->default = $default;
    }
    
    public function getDefault() {
        return $this->default;
    }
    
    /**
     * Checks if a property is subject to the rule $rule
     * 
     * @param string $rule
     * @return boolean Returns true if it is, false otherwise
     */
    public function isSubjectTo($rule) {
        return in_array($rule, $this->rules);
    }
    
    public function setCallbacks(array $callbacks) {
        if (!empty($callbacks)) {
        	foreach ($callbacks as $event => $callback) {
	            $this->callbacks[strtolower($event)] = $callback;
        	}
        }
    }
    
    public function getCallback($event) {
    	$action = strtolower($event);
    	if (array_key_exists($action, $this->callbacks)) {
    		return $this->callbacks[$action];
    	}
    }
    
    public function setCallback($event, $methodName) {
        $this->callbacks[strtolower($event)] = $methodName;
    }
    
    /**
     * Checks if a property has a callback for the event $event
     * 
     * @param string $event
     * @return boolean Returns true if it does, false otherwise
     */
    public function hasCallbackForEvent($event) {
        return array_key_exists(strtolower($event), $this->callbacks);
    }

}
