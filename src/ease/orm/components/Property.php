<?php

namespace Ease\Orm\Components;

/**
 * Property
 *
 * @author Bandu
 */
class Property extends ClassComponent {
    
    const RULE_RESOURCE_REFERENCE = 4;
    
    protected $field;
    protected $resourceManager;
    protected $referencedProperty;

    public function setField($field) {
        if (!empty($field)) {
            $this->field = $field;
        }
    }
    
    public function setReferencedProperty($propertyName) {
        $this->referencedProperty = $propertyName;
    }
    
    public function setResourceManager(\Ease\Orm\ResourceManager $resourceManager) {
        $this->resourceManager = $resourceManager;
    }

    public function isValid() {
        return isset($this->name);
    }
    
    public function getField() {
        return ($this->field) ? $this->field : $this->name;
    }
    
    /**
     * 
     * @return \Ease\Orm\ResourceManager
     */
    public function getResourceManager() {
        return $this->resourceManager;
    }
    
    public function getReferencedProperty() {
        return $this->referencedProperty;
    }
    
}
