<?php

namespace Ease\Orm\Components;

/**
 * Collection
 *
 * @author Bandu
 */
class Collection extends ClassComponent {
    
    protected $manager;
    protected $criteria;
    protected $order;

    public function isValid() {
        return true;
    }
    
    public function setManager($manager) {
        if (!empty($manager)) {
            $this->manager = $manager;
        }
    }
    
    public function getManager() {
        return $this->manager;
    }
    
    public function setCriteria(array $criteria) {
        if (!empty($criteria)) {
            $this->criteria = $criteria;
        }
    }
    
    public function getCriteria() {
        return $this->criteria;
    }
    
    public function setOrder(array $order) {
        if (!empty($order)) {
            $this->order = $order;
        }
    }
    
    public function getOrder() {
        return $this->order;
    }

}
