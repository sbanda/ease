<?php

namespace Ease\Orm\Components;

/**
 * Association
 *
 * @author Bandu
 */
class Association extends ClassComponent {

    /**
     * The table holding the data the association references
     *
     * @var string
     */
    protected $table;
    
    /**
     * The fields containing the table data to be returned
     *
     * @var array 
     */
    protected $fields;
    
    /**
     * The criteria on which to filter data from the table
     * 
     * @var array
     */
    protected $criteria;
    
    /**
     * The search rules for this table
     *
     * @var array
     */
    protected $searchRules;

    public function isValid() {
        $required = array('name', 'table', 'fields', 'criteria');
        foreach ($required as $req) {
            if (!isset($this->$req)) {
                return false;
            }
        }
        return true;
    }
    
    public function setTable($table) {
        if (!empty($table)) {
            $this->table = $table;
        }
    }
    
    public function getTable() {
    	return $this->table;
    }
    
    public function setFields(array $fields) {
        if (!empty($fields)) {
            $this->fields = $fields;
        }
    }
    
    public function getFields() {
        return $this->fields;
    }
    
    /**
     * Creates the field list for the select query string
     * 
     * @return string
     */
    public function getSelectFields() {
        $queryFields = array();
        foreach ($this->fields as $key => $value) {
            if (is_string($key)) {
                $queryFields[] = sprintf("%s AS %s", $key, $value);
            } else {
                $queryFields[] = $value;
            }
        }
        return implode(", ", $queryFields);
    }
    
    public function getInsertFields() {
    	$queryFields = array_keys($this->criteria);
    	foreach ($this->fields as $key => $value) {
    		if (is_int($key)) {
    			$queryFields[] = $value;
    		} else {
    			$queryFields[] = $key;
    		}
    	}
    	return $queryFields;
    }
    
    public function setCriteria(array $criteria) {
        if (!empty($criteria)) {
            $this->criteria = $criteria;
        }
    }
    
    public function getCriteria() {
    	return $this->criteria;
    }
    
    public function setSearchRules(array $searchRules) {
        if (!empty($searchRules)) {
            $this->searchRules = $searchRules;
        }
    }
    
    public function getSearchRules() {
        return $this->searchRules;
    }
    
    public function isSearchable() {
        return !empty($this->searchRules);
    }
    
    public function hasSearchKeyField() {
        return array_key_exists('keyField', $this->searchRules);
    }
    
    public function getSearchKeyField() {
        return $this->searchRules['keyField'];
    }
    
    public function hasSearchKeyFieldValue() {
        return array_key_exists('keyFieldValue', $this->searchRules);
    }
    
    public function getSearchKeyFieldValue() {
        return $this->searchRules['keyFieldValue'];
    }
    
    public function getSearchValueField() {
        return $this->searchRules['valueField'];
    }
    
}
