<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Ease\Routing;

/**
 * Description of EasyRouteManager
 *
 * @author sbanda
 */
class EasyRouteManager {

    /**
     *
     * @var \Ease\Utils\Environment
     */
    static protected $environment;
    static protected $routes;

    public function getEnvironment() {
        return self::$environment;
    }

    public function setEnvironment($environment) {
        self::$environment = $environment;
    }

    public function getRoutes() {
        return self::$routes;
    }

    public function setRoutes(array $routes) {
        self::$routes = $routes;
    }

    public function getControllerClass() {
        $service = strtolower(self::$environment->getParam('service'));
        if (array_key_exists($service, self::$routes['default'])) {
            return self::$routes['default'][$service];
        }
        throw new \Exception("Unknown Service: $service");
    }

}
