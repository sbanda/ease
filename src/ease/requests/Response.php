<?php

namespace Ease\Requests;

class Response {

    /**
     * 
     * @var array
     */
    protected $headers;

    /**
     * 
     * @var string
     */
    protected $output;

    public function getOutput() {
        return $this->output;
    }

    public function setOutput($output) {
        $this->output = $output;
    }

    public function appendToOutput($content) {
        $this->output.= $content;
    }

    public function prependToOutput($content) {
        $content.= $this->output;
        $this->output = $content;
    }

    public function getHeaders() {
        return $this->headers;
    }

    public function setHeaders(array $headers) {
        $this->headers = $headers;
    }

    public function setHeader($name, $value) {
        $this->headers[$name] = $value;
    }

    public function unsetHeader($name) {
        if (array_key_exists($name, $this->headers)) {
            unset($this->headers[$name]);
        }
    }

    public function getHeader($name) {
        if (array_key_exists($name, $this->headers)) {
            return $this->headers[$name];
        }
    }

    /**
     * 
     * @return \Ease\Requests\Response
     */
    public function setResponseHeaders() {
        if (!empty($this->headers)) {
            foreach ($this->headers as $name => $value) {
                header(sprintf("%s: %s", $name, $value));
            }
        }
        return $this;
    }

}
