<?php

namespace Ease\Requests\Handlers;

Interface EaseHandler {

    public function getResponse();

    /**
     * 
     * @throws \Exception
     */
    public function authenticateRequest();

    public function getController();
}
