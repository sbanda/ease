<?php

namespace Ease\Requests\Handlers;

use Ease\Requests\Controllers\RESTfulController;

class EasyHandler implements EaseHandler {

    protected $environment;
    protected $response;

    public function __construct(\Ease\Utils\Environment $environment, \Ease\Requests\Response $response) {
        $this->environment = $environment;
        $this->response = $response;
        $this->init();
    }

    protected function init() {
        $this->authenticateRequest();
    }

    /**
     * 
     * @return \Ease\Requests\Response An Ease Response object
     * @throws \Exception
     */
    public function getResponse() {
        $controller = $this->getController();
        if ($controller->requiresAuthentication() && !$this->requestIsAuthenticated()) {
            throw $this->environment->get('auth_exception');
        }
        return $controller->handleRequest();
    }

    /**
     * 
     * @return RESTfulController
     */
    public function getController() {
        $routingManager = $this->environment->getRoutingManager();
        $controllerClass = $routingManager->getControllerClass();
        $controller = new $controllerClass($this->environment, $this->response);
        return $controller;
    }

    public function authenticateRequest() {
        if ($this->environment->getAuthSetting('authentication')) {
            error_log("Authentication is on");
            $authManagerClass = $this->environment->getAuthSetting('authentication_manager');
            $authManager = new $authManagerClass($this->environment);        
            $authManager->authenticateRequest();
        } else {
            error_log("Proceeding with no authentication");
        }
        return true;
    }

}
