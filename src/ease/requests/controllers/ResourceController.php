<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Ease\Requests\Controllers;

/**
 * Description of ResourceController
 *
 * @author sbanda
 */
abstract class ResourceController extends EasyController {

    /**
     * @return \Ease\Orm\ResourceManager A concrete resource manager
     */
    abstract protected function getResourceManager();
    
}
