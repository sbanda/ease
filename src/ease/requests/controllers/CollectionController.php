<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Ease\Requests\Controllers;

/**
 * Description of CollectionController
 *
 * @author sbanda
 */
abstract class CollectionController extends ResourceController {

    public function handleDelete() {
        throw new \Exception("Method Not Allowed");
    }

    public function handleGet() {
        $rm = $this->getResourceManager();
        
        foreach (array('limit', 'offset') as $option) {
            if ($this->environment->getParam($option)) {
                $rm->setSearchOption($option, $this->environment->getParam($option));
            }
        }
        
        if ($this->environment->getParam('order')) {
            if ($order = json_decode($this->environment->getParam('order'), true)) {
                $rm->setSearchOption('order', $order);
            } else {
                $rm->setSearchOption('order', $this->environment->getParam('order'));
            }
        }

        if (!$this->environment->getParam('q')) {
            $collection = $rm->fetchAll();
        } else {
            $criteria = json_decode($this->environment->getParam('q'), true);            
            $collection = $rm->search($criteria);
        }

        $export = array();
        
        foreach ($collection as $resource) {
            $export[] = $resource->render();
        }

        $this->response->setHeader('Content-Type', "application/json");
        $this->response->setOutput("[" . implode(",", $export) . "]");
        return $this->response;
    }

    public function handlePost() {
        throw new \Exception("Method Not Allowed");
    }

    public function handlePut() {
        throw new \Exception("Method Not Allowed");
    }

}
