<?php

namespace Ease\Requests\Controllers;

abstract class EasyController implements RESTfulController {

    const REQUEST_METHOD_GET = "GET";
    const REQUEST_METHOD_POST = "POST";
    const REQUEST_METHOD_PUT = "PUT";
    const REQUEST_METHOD_DELETE = "DELETE";

    /**
     *
     * @var \Ease\Utils\Environment
     */
    protected $environment;

    /**
     * 
     * @var \Ease\Requests\Response
     */
    protected $response;

    public function __construct(\Ease\Utils\Environment $environment, \Ease\Requests\Response $response) {
        $this->environment = $environment;
        $this->response = $response;
        $this->init();
    }
    
    protected function init() {
        
    }

    public function requiresAuthentication() {
        return false;
    }

    public function handleRequest() {
        $requestMethod = strtoupper($this->environment->serverParam('REQUEST_METHOD'));
        switch ($requestMethod) {
            case self::REQUEST_METHOD_GET:
                return $this->handleGet();
                break;
            case self::REQUEST_METHOD_POST:
                return $this->handlePost();
                break;
            case self::REQUEST_METHOD_PUT:
                return $this->handlePut();
                break;
            case self::REQUEST_METHOD_DELETE:
                return $this->handleDelete();
                break;
            case "OPTIONS":
                return $this->response;
            default:
                throw new \Exception("Method not supported: $requestMethod");
                break;
        }
    }

}
