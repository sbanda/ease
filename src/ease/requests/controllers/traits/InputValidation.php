<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Ease\Requests\Controllers\Traits;

/**
 * Description of InputValidation
 *
 * @author sbanda
 */
Trait InputValidation {
    
    protected static $REQUIRED_FIELDS = array();
    
    protected $input;
    
    protected function validateInput($payload) {
        $this->input = json_decode($payload, true);
        if (!is_array($this->input)) {
            throw new \Exception("No data provided");
        }
        
        foreach (self::$REQUIRED_FIELDS as $property) {
            $missingFields = array();
            if (!array_key_exists($property, $this->input)) {
                $missingFields[] = $property;
            }
        }
        
        if (!empty($missingFields)) {
            $message = "Missing required data: ".implode(", ", $missingFields);
            throw new \Exception($message);
        }        
    }

}