<?php

namespace Ease\Requests\Controllers;

Interface RESTfulController {

    public function requiresAuthentication();

    public function handleRequest();

    public function handleGet();

    public function handlePost();

    public function handlePut();

    public function handleDelete();
}
