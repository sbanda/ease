<?php

namespace Ease\Base;

/**
 * Description of SettingsManager
 *
 * @author Bandu
 */
abstract class SettingsManager extends \Ease\Base\Singleton {

    public function setSettings() {

    }

    public function loadSettings(array $settings) {
        static::$settings = $settings;
    }

    public function hasSetting($name) {
        return array_key_exists($name, static::$settings);
    }

    public function getSetting($name) {
        if ($this->hasSetting($name)) {
            return static::$settings[$name];
        }
    }

    public function setSetting($name, $value) {
        static::$settings[$name] = $value;
    }

}
