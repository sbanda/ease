<?php

namespace Ease\Base;

/**
 * Description of Singleton
 *
 * @author Bandu
 */
abstract class Singleton {

    protected function __construct() {
    }

    protected function __clone() {
    }

    /**
     * 
     * @return static
     */
    public static function getInstance() {
        if (!static::hasInstance()) {
            static::setInstance(new static);
        }
        return static::getObject();
    }

    protected static function getObject() {
        return static::$instance;
    }

    protected static function hasInstance() {
        return !is_null(static::$instance);
    }

    protected static function setInstance($instance) {
        static::$instance = $instance;
    }


}
