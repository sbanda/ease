<?php

namespace Ease\Utils;

/**
 * DatabaseSettingsManager
 *
 * @author Bandu
 */
class DatabaseSettingsManager extends \Ease\Base\SettingsManager {

    protected static $settings = array();
    protected static $instance = null;
    protected static $connections = array();

    /**
     * 
     * @param type $name
     * @return \Bandu\Database\MySQLWrapper
     * @throws Exception
     */
    public function getConnection($name) {
        if (!array_key_exists($name, self::$connections)) {
            if (!array_key_exists($name, self::$settings)) {
                throw new \Exception("Unknown Connection: $name");
            }
            $credentials = self::$settings[$name];
            error_log("Connection Name: $name");
            error_log("Database: ".$credentials['db']);
            error_log("User: ".$credentials['user']);
            self::$connections[$name] = new \Ease\Database\MySQLWrapper($credentials);
        }
        return self::$connections[$name];
    }

}
