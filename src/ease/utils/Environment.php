<?php

namespace Ease\Utils;

class Environment {

    /**
     * 
     * @var array Contains server and request information.
     */
    static protected $server;
    
    /**
     *
     * @var array Request headers
     */
    static protected $headers;

    /**
     * 
     * @var array Contains url parameters from the current request
     */
    static protected $get;

    /**
     * 
     * @var array Contains post data from the current request
     */
    static protected $post;

    /**
     * 
     * @var array Contains files uploaded with the current request
     */
    static protected $files;
    
    /**
     *
     * @var string  Contains the raw post data included with the current request 
     */
    static protected $rawPostData;

    /**
     * 
     * @var array Contains data that should be accessible throughout the system
     */
    static protected $extras;
    
    /**
     *
     * @var array Contains the system settings data
     */
    static protected $settings;
    
    /**
     *
     * @var \Ease\Routing\RoutingManager Returns the routing manager object
     */
    static protected $routingManager;

    public function __construct(array $settings = NULL) {
        if (!is_null($settings)) {
            $this->init($settings);
        }
    }

    protected function init(array $settings) {
        foreach ($settings as $key => $values) {
            $setter = "set" . ucfirst($key);
            if (method_exists($this, $setter)) {
                $this->$setter($values);
            } else {
                self::$extras[$key] = $values;
            }
        }
    }

    public function setServer(array $server) {
        self::$server = $server;
    }

    public function getServer() {
        return self::$server;
    }

    public function setHeaders(array $headers) {
        self::$headers = $headers;
    }
    
    public function getHeaders() {
        return self::$headers;
    }
    
    public function getHeader($name) {
        if (array_key_exists($name, self::$headers)) {
            return self::$headers[$name];
        } 
    }

    public function setGet(array $get) {
        self::$get = $get;
    }
    
    public function getGet() {
        return self::$get;
    }

    public function setPost(array $post) {
        self::$post = $post;
    }

    public function getPost() {
        return self::$post;
    }

    public function setFiles(array $files) {
        self::$files = $files;
    }

    public function getFiles() {
        return self::$files;
    }
    
    public function setRawPostData($rawPostData) {
        self::$rawPostData = $rawPostData;
    }
    
    public function getRawPostdata() {
        return self::$rawPostData;
    }

    public function getExtras() {
        return self::$extras;
    }
    
    public function serverParam($name) {
        if (array_key_exists($name, self::$server)) {
            return self::$server[$name];
        }
    }

    public function getParam($name) {
        if (array_key_exists($name, self::$get)) {
            return self::$get[$name];
        }
    }

    public function postParam($name) {
        if (array_key_exists($name, self::$post)) {
            return self::$post[$name];
        }
    }

    public function get($key) {
        if (array_key_exists($key, self::$extras)) {
            return self::$extras[$key];
        }
    }

    public function set($key, $value) {
        self::$extras[$key] = $value;
    }
    
    public function setSettings(array $settings) {
        self::$settings = $settings;
    }
    
    public function getPath($name) {
        if (array_key_exists($name, self::$settings['paths'])) {
            return self::$settings['paths'][$name];
        }
    }
    
    public function getConfig($name) {
        if (array_key_exists($name, self::$settings['config'])) {
            return self::$settings['config'][$name];
        }
    }
    
    public function getAuthSetting($name) {
        if (array_key_exists($name, self::$settings['auth'])) {
            return self::$settings['auth'][$name];
        }
    }
    
    public function getSystemHeader($name) {
        if (array_key_exists($name, self::$settings['headers'])) {
            return self::$settings['headers'][$name];
        }
    }
    
    public function getRoutingManager() {
        return self::$routingManager;
    }
    
    public function setRoutingManager($routingManager) {
        self::$routingManager = $routingManager;
    }

}
