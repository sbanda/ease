# Easy Application Service Engine #

This is a lightweight framework designed for the rapid development of RESTful APIs using **PHP (5.4+)** and **MySQL**.

It emphasises on:

* Ease of application development
* Encouraging RESTful implementation of code and service oriented architectures (SOA)
* Full understanding of the underlying framework for easy extension and customisation

## Getting set up ##

Getting set up is easy. You will require:

* A server running PHP(5.4+)
* A server running MySQL (if your application needs to connect to a database - this can be the same server running PHP)
* Access to the vhost file or .htaccess files and permission to create environment variables

Once you have access to all of the above, follow these steps.

1. Copy (Clone/Fork/Download) the repository onto your server and note the location - I will refer to the absolute path to this location as **$ROOT**.

2. Set the document root of your domain to $ROOT/www.

3. In $ROOT/settings copy example and rename it for your project. For example, the dev tier of the ease project might use the **$ROOT/settings/ease-dev** as the folder name. This name will be referred to as **$EASE_ENV**

4. Open system.ini and replace the placeholder data with the actual data for your server.

5. In the vhost file or the .htaccess file create an environment variable called **EASE_ENVIRONMENT** and set it's value to $EASE_ENV. Running apache, you would add the following line:

SetEnv EASE_ENVIRONMENT **$EASE_ENV**

Following step 3, this would be:

SetEnv EASE_ENVIRONMENT "ease-dev"

This tells ease to use this settings directory when it bootstraps the application.

### Now it's up to you... ###

At this point your instance is ready to start handling requests - you just need to tell it how to handle which requests.

To do this you'll have to create a controller which handles requests sent to a given endpoint.

### Creating a controller ###

Creating a controller is easy too! Here's an example that would be found in $ROOT/lib/vendor/example/users/controllers/UsersController.php:


```
#!php

<?php

namespace Example\Users\Controllers;

/**
 * UsersController
 *
 * @author sbanda
 */
class UsersController extends \Ease\Requests\Controllers\EasyController {
    
    static $USERS = [
        [
            'id' => 1,
            'username' => "aBoyNamedSu"
        ],
    ];
    
    public function handleDelete() {
        
    }

    public function handleGet() {
        $this->response->setHeader('Content-Type', "application/json");
        $this->response->setOutput(json_encode(self::$USERS));
        return $this->response;
    }

    public function handlePost() {
        
    }

    public function handlePut() {
        
    }

}

```

### Routing requests to a controller ###

Once you've create a controller, you can route a request to it by adding a route to the routing table in $ROOT/www/index.php


```
#!php

//create a simple routing table
$environmentRoutes = array(
    'users' => "\Example\Users\Controllers\UsersController",
);


```

Now access the engine in your browser with the query string ?service=users.

## Questions or Queries ##

If you have any questions or want to talk to me about anything, raise an issue or send a tweet to @Suhmayah